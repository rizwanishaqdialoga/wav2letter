# -*- coding: utf-8 -*-
#/usr/bin/python3
'''
This code is taken from
    By kyubyong park. kbpark.linguist@gmail.com.
    https://www.github.com/kyubyong/tacotron_asr
'''

from __future__ import print_function
import tensorflow as tf


def normalize(inputs,
              type="bn",
              decay=.99,
              is_training=True,
              activation_fn=None,
              scope="normalize"):
    '''Applies {batch|layer} normalization.

    Args:
      inputs: A tensor with 2 or more dimensions, where the first dimension has
        `batch_size`. If type is `bn`, the normalization is over all but
        the last dimension. Or if type is `ln`, the normalization is over
        the last dimension. Note that this is different from the native
        `tf.contrib.layers.batch_norm`. For this I recommend you change
        a line in ``tensorflow/contrib/layers/python/layers/layer.py`
        as follows.
        Before: mean, variance = nn.moments(inputs, axis, keep_dims=True)
        After: mean, variance = nn.moments(inputs, [-1], keep_dims=True)
      type: A string. Either "bn" or "ln".
      decay: Decay for the moving average. Reasonable values for `decay` are close
        to 1.0, typically in the multiple-nines range: 0.999, 0.99, 0.9, etc.
        Lower `decay` value (recommend trying `decay`=0.9) if model experiences
        reasonably good training performance but poor validation and/or test
        performance.
      is_training: Whether or not the layer is in training mode. W
      activation_fn: Activation function.
      scope: Optional scope for `variable_scope`.

    Returns:
      A tensor with the same shape and data dtype as `inputs`.
    '''
    if type=="bn":
        inputs_shape = inputs.get_shape()
        inputs_rank = inputs_shape.ndims

        # use fused batch norm if inputs_rank in [2, 3, 4] as it is much faster.
        # pay attention to the fact that fused_batch_norm requires shape to be rank 4 of NHWC.
        if inputs_rank in [2, 3, 4]:
            if inputs_rank==2:
                inputs = tf.expand_dims(inputs, axis=1)
                inputs = tf.expand_dims(inputs, axis=2)
            elif inputs_rank==3:
                inputs = tf.expand_dims(inputs, axis=1)

            outputs = tf.contrib.layers.batch_norm(inputs=inputs,
                                               decay=decay,
                                               center=True,
                                               scale=True,
                                               activation_fn=activation_fn,
                                               updates_collections=None,
                                               is_training=is_training,
                                               scope=scope,
                                               zero_debias_moving_mean=True,
                                               fused=True)
            # restore original shape
            if inputs_rank==2:
                outputs = tf.squeeze(outputs, axis=[1, 2])
            elif inputs_rank==3:
                outputs = tf.squeeze(outputs, axis=1)
        else: # fallback to naive batch norm
            outputs = tf.contrib.layers.batch_norm(inputs=inputs,
                                               decay=decay,
                                               center=True,
                                               scale=True,
                                               activation_fn=activation_fn,
                                               updates_collections=None,
                                               is_training=is_training,
                                               scope=scope,
                                               fused=False)
    elif type=="ln":
        outputs = tf.contrib.layers.layer_norm(inputs=inputs,
                                            center=True,
                                            scale=True,
                                            activation_fn=activation_fn,
                                            scope=scope)
    elif type == "in":
        with tf.variable_scope(scope):
            batch, steps, channels = inputs.get_shape().as_list()
            var_shape = [channels]
            mu, sigma_sq = tf.nn.moments(inputs, [1], keep_dims=True)
            shift = tf.Variable(tf.zeros(var_shape))
            scale = tf.Variable(tf.ones(var_shape))
            epsilon = 1e-8
            normalized = (inputs - mu) / (sigma_sq + epsilon) ** (.5)
            outputs = scale * normalized + shift
            if activation_fn:
                outputs = activation_fn(outputs)
    else:
        raise ValueError("Currently we support `bn` or `ln` only.")

    return outputs

def conv1d(inputs,
           filters=None,
           size=1,
           rate=1,
           padding="SAME",
           use_bias=False,
           activation_fn=None,
           scope="conv1d",
           reuse=None):
    '''
    Args:
      inputs: A 3-D tensor with shape of [batch, time, depth].
      filters: An int. Number of outputs (=activation maps)
      size: An int. Filter size.
      rate: An int. Dilation rate.
      padding: Either `same` or `valid` or `causal` (case-insensitive).
      use_bias: A boolean.
      scope: Optional scope for `variable_scope`.
      reuse: Boolean, whether to reuse the weights of a previous layer
        by the same name.

    Returns:
      A masked tensor of the same shape and dtypes as `inputs`.
    '''

    with tf.variable_scope(scope):
        if padding.lower()=="causal":
            # pre-padding for causality
            pad_len = (size - 1) * rate  # padding size
            inputs = tf.pad(inputs, [[0, 0], [pad_len, 0], [0, 0]])
            padding = "valid"

        if filters is None:
            filters = inputs.get_shape().as_list[-1]

        params = {"inputs":inputs, "filters":filters, "kernel_size":size,
                "dilation_rate":rate, "padding":padding, "activation":activation_fn,
                "use_bias":use_bias, "reuse":reuse}

        outputs = tf.layers.conv1d(**params)
    return outputs

def conv1d_banks(inputs, num_features, K=16, is_training=True, scope="conv1d_banks", reuse=None):
    '''Applies a series of conv1d separately.

    Args:
      inputs: A 3d tensor with shape of [N, T, C]
      K: An int. The size of conv1d banks. That is,
        The `inputs` are convolved with K filters: 1, 2, ..., K.
      is_training: A boolean. This is passed to an argument of `batch_normalize`.

    Returns:
      A 3d tensor with shape of [N, T, K*Hp.embed_size//2].
    '''
    with tf.variable_scope(scope, reuse=reuse):
        outputs = conv1d(inputs, num_features//2, 1) # k=1
        outputs = normalize(outputs, type="in", is_training=is_training,
                            activation_fn=tf.nn.relu)
        for k in range(2, K+1): # k = 2...K
            with tf.variable_scope("num_{}".format(k)):
                output = conv1d(inputs, num_features//2, k)
                output = normalize(output, type="in", is_training=is_training,
                            activation_fn=tf.nn.relu)
                outputs = tf.concat((outputs, output), -1)
    return outputs # (N, T, Hp.embed_size//2*K)


def prenet(inputs, num_features, is_training=True, scope="prenet", reuse=None):
    '''Prenet for Encoder and Decoder.
    Args:
      inputs: A 3D tensor of shape [N, T, num_features].
      scope: Optional scope for `variable_scope`.
      reuse: Boolean, whether to reuse the weights of a previous layer
        by the same name.

    Returns:
      A 3D tensor of shape [N, T, num_units/2].
    '''
    with tf.variable_scope(scope, reuse=reuse):
        outputs = tf.layers.dense(inputs, units=num_features, activation=tf.nn.relu, name="dense1")
        outputs = tf.nn.dropout(outputs, keep_prob=.5 if is_training==True else 1., name="dropout1")
        outputs = tf.layers.dense(outputs, units=num_features//2, activation=tf.nn.relu, name="dense2")
        outputs = tf.nn.dropout(outputs, keep_prob=.5 if is_training==True else 1., name="dropout2")
    return outputs # (N, T, num_units/2)

def highwaynet(inputs, num_units=None, scope="highwaynet", reuse=None):
    '''Highway networks, see https://arxiv.org/abs/1505.00387

    Args:
      inputs: A 3D tensor of shape [N, T, W].
      num_units: An int or `None`. Specifies the number of units in the highway layer
             or uses the input size if `None`.
      scope: Optional scope for `variable_scope`.
      reuse: Boolean, whether to reuse the weights of a previous layer
        by the same name.

    Returns:
      A 3D tensor of shape [N, T, W].
    '''
    if not num_units:
        num_units = inputs.get_shape()[-1]

    with tf.variable_scope(scope, reuse=reuse):
        H = tf.layers.dense(inputs, units=num_units, activation=tf.nn.relu, name="dense1")
        T = tf.layers.dense(inputs, units=num_units, activation=tf.nn.sigmoid, name="dense2")
        C = 1. - T
        outputs = H * T + inputs * C
    return outputs


def CBHG(inputs, num_features, is_training):
    prenet_out = prenet(inputs,num_features, scope="prenet", is_training=is_training)
    # Encoder CBHG
    ## Conv1D bank
    enc = conv1d_banks(prenet_out, num_features, K=16, is_training=is_training) # (N, T, K * E / 2)
    ### Max pooling
    enc = tf.layers.max_pooling1d(enc, 2, 1, padding="same")  # (N, T, K * E / 2)
    ### Conv1D projections
    enc = conv1d(enc, num_features//2, 3, scope="conv1d_1") # (N, T, E/2)
    enc = normalize(enc, type="in", is_training=is_training,
                        activation_fn=tf.nn.relu)
    enc = conv1d(enc, num_features//2, 3, scope="conv1d_2") # (N, T, E/2)
    enc = normalize(enc, type="in", is_training=is_training,
                        activation_fn=None)
    enc += prenet_out # (N, T, E/2) # residual connections

    ### Highway Nets
    for i in range(4):
        enc = highwaynet(enc, num_units=num_features//2,
                        scope='highwaynet_{}'.format(i)) # (N, T, E/2)


    return enc
